import 'package:cash_flow/models/cash_flow/totals_model.dart';

abstract class TotalsEvent {}

class LoadTotalsEvent extends TotalsEvent {}

class AddTotalsEvent extends TotalsEvent {
  Totals totals;

  AddTotalsEvent({
    required this.totals,
  });
}

class RemoveTotalsEvent extends TotalsEvent {
  Totals totals;

  RemoveTotalsEvent({
    required this.totals,
  });
}