import 'package:bloc/bloc.dart';
import 'package:cash_flow/blocs/totals_events.dart';
import 'package:cash_flow/blocs/totals_state.dart';
import 'package:cash_flow/repositories/cash_flow/totals_repository.dart';

class TotalsBloc extends Bloc<TotalsEvent, TotalsState> {
  final _totalsRepository = TotalsRepository();

  TotalsBloc() : super(TotalsInitState()) {
    on<LoadTotalsEvent>(
      (event, emit) async => emit(TotalsSuccessState(
          totals: await _totalsRepository.calculateTotals())),
    );
  }
}
