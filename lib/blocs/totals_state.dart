import 'package:cash_flow/models/cash_flow/totals_model.dart';

abstract class TotalsState {
  Totals totals;

  TotalsState({
    required this.totals,
  });
}

class TotalsInitState extends TotalsState {
  TotalsInitState() : super(totals: Totals(credit: 0, debit: 0, total: 0));
}

class TotalsSuccessState extends TotalsState {
  TotalsSuccessState({required Totals totals}) : super(totals: totals);
}