class Account {
  final int id;
  String description;

  Account({
    required this.id,
    required this.description,
  });

  factory Account.fromJson(Map<String, dynamic> account) {
    try {
      return Account(
        id: account['id'],
        description: account['description'],
      );
    } catch (e) {
      throw "There was an error decoding the Account data.\n$e";
    }
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'description': description,
    };
  }
}