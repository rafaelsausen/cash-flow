class Totals {
  double credit;
  double debit;
  double total;

  Totals({
    required this.credit,
    required this.debit,
    required this.total,
  });
}
