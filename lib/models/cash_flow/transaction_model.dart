import 'package:cash_flow/constants/text_constants.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Transaction {
  int id;
  String description;
  double debit;
  double credit;
  int account;
  int category;
  DateTime date;
  bool transferBetweenAccounts;

  Transaction({
    required this.id,
    required this.description,
    this.debit = 0,
    this.credit = 0,
    required this.account,
    required this.category,
    required this.date,
    this.transferBetweenAccounts = false,
  });

  factory Transaction.fromJson(Map<String, dynamic> transaction) {
    try {
      return Transaction(
          id: transaction[columnId],
          description: transaction[columnDescription],
          credit: double.parse(transaction[columnCredit].toString()),
          debit: double.parse(transaction[columnDebit].toString()),
          account: transaction[columnAccount],
          category: transaction[columnCategory],
          date: DateTime.fromMillisecondsSinceEpoch(transaction[columnDate]),
          transferBetweenAccounts: transaction[columnTransferBetweenAccounts] == 1 ? true : false);
    } catch (e) {
      throw "There was an error decoding the Transaction data.\n$e";
    }
  }

  Map<String, dynamic> toJson() {
    return {
      columnId: id,
      columnDescription: description,
      columnDebit: debit,
      columnCredit: credit,
      columnAccount: account,
      columnCategory: category,
      columnDate: Timestamp.fromDate(date).millisecondsSinceEpoch,
      columnTransferBetweenAccounts:
          transferBetweenAccounts ? 1 : 0,
    };
  }
}

newTransaction() {
  try {
    return Transaction(
      id: 0,
      description: '',
      account: 1,
      category: 1,
      date: DateTime.now(),
    );
  } catch (error) {
    throw "Error to create a new Transaction: " + error.toString();
  }
}
