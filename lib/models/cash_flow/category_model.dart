class Category {
  final int id;
  String description;

  Category({
    required this.id,
    required this.description,
  });

  factory Category.fromJson(Map<String, dynamic> category) {
    try {
      return Category(
        id: category['id'],
        description: category['description'],
      );
    } catch (e) {
      throw "There was an error decoding the Category data.\n$e";
    }
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'description': description,
    };
  }
}