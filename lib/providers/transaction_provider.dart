import 'dart:io';

import 'package:cash_flow/constants/text_constants.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class TransactionProvider {
  static final _databaseName = dotenv.env['LOCAL_DATABASE'];
  static const _databaseVersion = 1;

  final String table = tableTransactions;

  TransactionProvider._privateConstructor();

  static final TransactionProvider instance =
      TransactionProvider._privateConstructor();

  static Database? _database;

  /* Verifica se a base de dados existe */
  Future<bool> databaseExists(String path) =>
      databaseFactory.databaseExists(path);

  /* Inicia a instância da base de dados */
  Future<Database> get database async => _database ??= await _initDatabase();

  /* Cria a base de dados no diretorio */
  Future<Database> _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();

    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(
      path,
      version: _databaseVersion,
      onCreate: _onCreate,
      //onUpgrade: _onUpgrade,
    );
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
          CREATE TABLE IF NOT EXISTS $table (
            $columnId INTEGER PRIMARY KEY,
            $columnDescription TEXT,
            $columnDebit REAL,
            $columnCredit REAL,
            $columnDate INTEGER,
            $columnCategory INTEGER,
            $columnAccount INTEGER,
            $columnTransferBetweenAccounts INTEGER
          )
          ''');
  }

  // Future _onUpgrade(db, oldVersion, newVersion) async {
  //   // run sql code for upgrade
  //   try {
  //     await db.execute('''
  //         ALTER TABLE $table ADD COLUMN
  //           $columnNotification INTEGER
  //         ''');
  //   } catch(e) {
  //     if (!e.toString().contains('duplicate column name:')) {
  //       debugPrint("Erro ao atualizar a tabela de configurações:" + e.toString());
  //     }
  //   }
  // }

  Future<int> insert(Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.insert(table, row);
  }

  Future<List<Map<String, Object?>>> queryAllRows() async {
    Database db = await instance.database;
    return await db.query(table);
  }

  Future<Map<dynamic, Object?>> queryRow({id}) async {
    Database db = await instance.database;

    List<Map> sql = await db.query(
      table,
      where: '$columnId = ?',
      whereArgs: [id],
    );

    if (sql.isNotEmpty) {
      return sql[0];
    }
    return {};
  }

  Future<int?> queryRowCount() async {
    Database db = await instance.database;
    List<Map<String, Object?>> sql =
        await db.rawQuery('SELECT COUNT(*) FROM $table');

    return Sqflite.firstIntValue(sql);
  }

  Future<Map<String, Object?>> queryColumnSum(String columnName) async {
    Database db = await instance.database;
    List<Map<String, Object?>> sql =
        await db.rawQuery('SELECT SUM($columnName) FROM $table');

    return sql[0];
  }

  /* Edita um registro */
  Future<void> update(Map<String, dynamic> row) async {
    /* Inicia o singleton com a instancia da conexao */
    Database db = await instance.database;
    /* Caso tenha sido informado o id */
    if (row[columnId] != null) {
      await db.update(table, row,
          where: '$columnId = ?', whereArgs: [row[columnId]]);
    } else {
      await db.update(table, row);
    }
  }

  /* Exclui um registro */
  Future<void> delete(String id) async {
    /* Inicia o singleton com a instancia da conexao */
    Database db = await instance.database;
    /* Caso tenha sido informado o id */
    await db.delete(table, where: '$columnId = ?', whereArgs: [id]);
  }
}
