import 'package:flutter/material.dart';

// ignore: must_be_immutable
class RBCheckbox extends StatefulWidget {
  RBCheckbox({
    required this.text,
    required this.isChecked,
    required this.onChangeFunction,
    Key? key,
  }) : super(key: key);

  Function(bool text) onChangeFunction;
  String text;
  bool isChecked;

  @override
  State<RBCheckbox> createState() => _RBCheckboxState();
}

class _RBCheckboxState extends State<RBCheckbox> {
  @override
  Widget build(BuildContext context) {
    Color getColor(Set<MaterialState> states) {
      const Set<MaterialState> interactiveStates = <MaterialState>{
        MaterialState.pressed,
        MaterialState.hovered,
        MaterialState.focused,
      };
      if (states.any(interactiveStates.contains)) {
        return Colors.blue;
      }
      return Colors.white;
    }

    return Row(
      children: [
        Checkbox(
          checkColor: Colors.black,
          fillColor: MaterialStateProperty.resolveWith(getColor),
          value: widget.isChecked,
          onChanged: (bool? value) {
            setState(() {
              widget.onChangeFunction(value!);
              widget.isChecked = value;
            });
          },
        ),
        Text(
          widget.text,
          style: const TextStyle(
            color: Colors.white,
            fontFamily: 'OpenSans',
          ),
        ),
      ],
    );
  }
}
