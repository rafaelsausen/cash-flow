import 'package:cash_flow/constants/layout_constants.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class RBButton extends StatefulWidget {
  RBButton({
    Key? key,
    required this.text,
    this.onPressed,
    this.iconStart,
    this.iconEnd,
    this.image,
    this.paddingButton,
  }) : super(
    key: key,
  );
  String text;
  VoidCallback? onPressed;
  IconData? iconStart;
  IconData? iconEnd;
  Image? image;
  EdgeInsets? paddingButton;

  @override
  _RBButtonState createState() => _RBButtonState();
}

class _RBButtonState extends State<RBButton> {
  bool loading = false;

  Padding buildLoading() {
    return const Padding(
      padding: EdgeInsets.all(16),
      child: SizedBox(
        width: 24,
        height: 24,
        child: CircularProgressIndicator(
          color: rbButtonText,
        ),
      ),
    );
  }

  Padding buildText() {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Text(
        widget.text,
        style: const TextStyle(
          color: rbButtonText,
          letterSpacing: 1.5,
          fontSize: 22.0,
          //fontWeight: FontWeight.bold,
          fontFamily: 'Roboto',
        ),
      ),
    );
  }

  Icon buildIconStart() {
    return Icon(
      widget.iconStart,
      color: rbButtonText,
    );
  }

  Icon buildIconEnd() {
    return Icon(
      widget.iconEnd,
      color: rbButtonText,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: widget.paddingButton ?? const EdgeInsets.all(20.0),
      width: double.infinity,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: rbButtonColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
        ),
        child: Row(
          mainAxisAlignment: widget.iconStart != null
              ? MainAxisAlignment.spaceBetween
              : MainAxisAlignment.center,
          children: (loading)
              ? [buildLoading()]
              : (widget.iconStart != null || widget.iconEnd != null)
              ? [buildIconStart(), buildText(), buildIconEnd()]
              : widget.image != null
              ? [widget.image!, buildText()]
              : [buildText()],
        ),
        onPressed: widget.onPressed ??
                () {
              setState(() => loading = true);
              //MasonSnackBar(context, "Em desenvolvimento", "alert");
              setState(() => loading = false);
            },
      ),
    );
  }
}
