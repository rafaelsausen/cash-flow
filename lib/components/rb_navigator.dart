import 'package:flutter/material.dart';

rbNavigator(BuildContext context, destiny) {
  Navigator.of(context).push(
    MaterialPageRoute(
      builder: (context) => destiny,
    ),
  );
}
