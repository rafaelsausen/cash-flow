import 'package:flutter/material.dart';

void rbSnackBar({required context, required String message, Color? color}) {
  try {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      backgroundColor: color ?? Colors.green,
      content: Text(message),
    ));
  } on Exception catch (e, s) {
    throw s;
  }
}
