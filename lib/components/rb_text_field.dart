import 'package:cash_flow/constants/layout_constants.dart';
import 'package:flutter/material.dart';

class RBTextField extends StatelessWidget {
  const RBTextField({
    required this.textEditingController,
    required this.hintText,
    this.icon,
    this.decoration,
    this.onTap,
    this.keyboardType,
    this.obscureText = false,
    Key? key,
  }) : super(key: key);

  final TextEditingController textEditingController;
  final String hintText;
  final IconData? icon;
  final Decoration? decoration;
  final VoidCallback? onTap;
  final TextInputType? keyboardType;
  final bool obscureText;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: decoration ?? rbBoxDecorationStyle,
          height: 60.0,
          child: TextField(
            controller: textEditingController,
            keyboardType: keyboardType,
            obscureText: obscureText,
            style: const TextStyle(
              color: rbBackgroundColor,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: const EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                icon,
                color: rbBackgroundColor,
              ),
              hintText: hintText,
              hintStyle: rbHintTextStyle,
            ),
            onTap: onTap,
          ),
        ),
      ],
    );
  }
}
