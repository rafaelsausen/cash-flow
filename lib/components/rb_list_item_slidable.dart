import 'package:cash_flow/blocs/totals_bloc.dart';
import 'package:cash_flow/blocs/totals_events.dart';
import 'package:cash_flow/components/rb_navigator.dart';
import 'package:cash_flow/helpers/rb_convertion_functions.dart';
import 'package:cash_flow/models/cash_flow/transaction_model.dart';
import 'package:cash_flow/repositories/cash_flow/transaction_repository.dart';
import 'package:cash_flow/screens/cash_flow/transaction/transaction_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ListItemSlidable extends StatefulWidget {
  const ListItemSlidable({
    required this.transaction,
    Key? key,
    required BuildContext context,
  }) : super(key: key);

  final Transaction transaction;

  @override
  _ListItemSlidableState createState() => _ListItemSlidableState();
}

class _ListItemSlidableState extends State<ListItemSlidable> {
  @override
  Widget build(BuildContext context) {
    return Slidable(
      // Specify a key if the Slidable is dismissible.
      key: const ValueKey(0),

      // The start action pane is the one at the left or the top side.
      startActionPane: ActionPane(
        // A motion is a widget used to control how the pane animates.
        motion: const ScrollMotion(),

        // A pane can dismiss the Slidable.
        //dismissible: DismissiblePane(onDismissed: () {}),

        // All actions are defined in the children parameter.
        children: [
          // A SlidableAction can have an icon and/or a label.
          SlidableAction(
            onPressed: (context) => deleteTransaction(widget.transaction),
            backgroundColor: const Color(0xFFFE4A49),
            foregroundColor: Colors.white,
            icon: Icons.delete,
            label: 'Delete',
          ),
          // SlidableAction(
          //   onPressed: doNothing,
          //   backgroundColor: const Color(0xFF21B7CA),
          //   foregroundColor: Colors.white,
          //   icon: Icons.share,
          //   label: 'Share',
          // ),
        ],
      ),

      // The end action pane is the one at the right or the bottom side.
      endActionPane: ActionPane(
        motion: const ScrollMotion(),
        children: [
          // SlidableAction(
          //   // An action can be bigger than the others.
          //   flex: 2,
          //   onPressed: doNothing,
          //   backgroundColor: const Color(0xFF7BC043),
          //   foregroundColor: Colors.white,
          //   icon: Icons.archive,
          //   label: 'Archive',
          // ),
          SlidableAction(
            onPressed: (context) => updateTransaction(widget.transaction),
            backgroundColor: const Color(0xFF0392CF),
            foregroundColor: Colors.white,
            icon: Icons.change_circle,
            label: 'change',
          ),
        ],
      ),

      // The child of the Slidable is what the user sees when the
      // component is not dragged.
      child: ListTile(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(widget.transaction.description),
            widget.transaction.credit > 0
                ? Text(
                    'R\$ ${widget.transaction.credit.toStringAsFixed(2)}',
                    style: const TextStyle(color: Colors.green),
                  )
                : Text(
                    'R\$ ${widget.transaction.debit.toStringAsFixed(2)}',
                    style: const TextStyle(color: Colors.red),
                  ),
          ],
        ),
        subtitle: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              convertDateToString(widget.transaction.date),
            )
          ],
        ),
      ),
    );
  }

  doNothing(BuildContext context) {}

  updateTransaction(Transaction transaction) {
    bool debit = false;
    if (transaction.debit > 0 ) {
      debit = true;
    }
    rbNavigator(
      context,
      TransactionForm(
        debit: debit,
        currentTransaction: transaction,
      ),
    );
  }

  deleteTransaction(Transaction transaction) async {
    await TransactionRepository().deleteTransaction(transaction.id.toString());
    context.read<TotalsBloc>().add(LoadTotalsEvent());
  }
}
