import 'package:cash_flow/constants/layout_constants.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class RBDropdownButton extends StatefulWidget {
  RBDropdownButton({
    required this.items,
    required this.onChangeFunction,
    required this.selectedValue,
    this.icon,
    Key? key,
  }) : super(key: key);

  final List<String> items;
  final Function(String? text) onChangeFunction;
  final IconData? icon;
  String selectedValue;

  @override
  _RBDropdownButtonState createState() => _RBDropdownButtonState();
}

class _RBDropdownButtonState extends State<RBDropdownButton> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: rbBoxDecorationStyle,
          height: 60.0,
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Icon(
                  widget.icon ?? Icons.arrow_drop_down,
                  color: rbBackgroundColor,
                ),
              ),
              Expanded(
                child: DropdownButton<String>(
                  isExpanded: true,
                  iconEnabledColor: Colors.white,
                  underline: Container(),
                  value: widget.selectedValue,
                  style: rbLabelTextStyle,
                  dropdownColor: rbPrimaryColor,
                  borderRadius: BorderRadius.circular(15.0),
                  //icon: const Icon(Icons.arrow_downward, color: Colors.black),
                  onChanged: (String? newValue) {
                    setState(() {
                      widget.onChangeFunction(newValue);
                      widget.selectedValue = newValue!;
                    });
                  },
                  items: widget.items.map<DropdownMenuItem<String>>(
                    (String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(
                          value,
                          style: const TextStyle(color: Colors.white),
                        ),
                      );
                    },
                  ).toList(),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
