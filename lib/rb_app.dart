import 'package:cash_flow/blocs/totals_bloc.dart';
import 'package:cash_flow/modules/login/services/auth_check.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

class RBApp extends StatelessWidget {
  const RBApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => TotalsBloc(),
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          colorScheme: const ColorScheme(
            brightness: Brightness.light,
            primary: Color(0xFF032D64),
            onPrimary: Color(0xFFC4B9B9),
            secondary: Color(0xFF24A117),
            onSecondary: Color(0xFFC4B9B9),
            error: Colors.red,
            onError: Colors.red,
            background: Color(0xFFF7FCF5),
            onBackground: Color(0xFFF7FCF5),
            surface: Color(0xFF898585),
            onSurface: Color(0xFF898585),
          ),
          //primaryColor: Color(0xFF002354),
          //primarySwatch: Colors.green,
        ),
        localizationsDelegates: GlobalMaterialLocalizations.delegates,
        supportedLocales: const [Locale('pt', 'BR')],
        home: const AuthCheck(),
      ),
    );
  }
}
