import 'package:cash_flow/modules/configurations/helpers/globals.dart';
import 'package:cash_flow/modules/login/screens/login.dart';
import 'package:cash_flow/modules/login/services/auth_service.dart';
import 'package:cash_flow/screens/main_menu.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AuthCheck extends StatefulWidget {
  const AuthCheck({Key? key}) : super(key: key);

  @override
  _AuthCheckState createState() => _AuthCheckState();
}

class _AuthCheckState extends State<AuthCheck> {
  @override
  Widget build(BuildContext context) {
    // Acessa o provider
    AuthService auth = Provider.of<AuthService>(context);

    if (auth.isLoading) {
      // Se está carregando
      return loading();
    } else if (auth.user == null) {
      // Se não está logado
      return const LoginView();
    } else {
      // Se está logado
      if (!auth.user!.emailVerified) {
        //Se o e-mail não está confirmado
        return const LoginView();
      }
      if (auth.user!.displayName != '' && auth.user!.displayName != null) {
        userName = auth.user!.displayName.toString();
      } else {
        userName = auth.user!.email.toString();
      }
      userEmail = auth.user!.email.toString();
      return const TransactionList();
    }
  }

  loading() {
    return const Scaffold(
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
