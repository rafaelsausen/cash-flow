import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class AuthException implements Exception {
  String message;

  AuthException(this.message);
}

class AuthService extends ChangeNotifier {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  User? user;
  bool isLoading = true;

  AuthService() {
    _authCheck();
  }

  _authCheck() {
    _auth.authStateChanges().listen((User? firebaseUser) async {
      user = (firebaseUser == null) ? null : firebaseUser;
      if (user != null && !user!.emailVerified) {
        await _sendEmailVerification();
      }
      isLoading = false;
      notifyListeners();
    });
  }

  _sendEmailVerification() async {
    try {
      await user!.sendEmailVerification();
    } on FirebaseAuthException catch (e) {
      throw AuthException(
          'Erro desconhecido durante o envio do e-mail de verificação!\n' +
              e.code);
    }
  }

  _getUser() {
    user = _auth.currentUser;
    notifyListeners();
  }

  register(String email, String password) async {
    try {
      await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      _getUser();
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        throw AuthException('A senha é muito fraca!');
      } else if (e.code == 'email-already-in-use') {
        throw AuthException('Este e-mail já está cadastrado!');
      } else if (e.code == 'invalid-email') {
        throw AuthException('Endereço de E-mail inválido!');
      } else {
        throw AuthException(
            'Erro desconhecido na criação do usuário!\n' + e.code);
      }
    }
  }

  login(String email, String password) async {
    try {
      await _auth.signInWithEmailAndPassword(email: email, password: password);
      if (!_auth.currentUser!.emailVerified) {
        throw AuthException('E-mail não validado, verifique a caixa de entrada do seu e-mail.');
      }
      _getUser();
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        throw AuthException('E-mail não encontrado. Cadastre-se!');
      } else if (e.code == 'wrong-password') {
        throw AuthException('Senha incorreta! Tente novamente.');
      } else if (e.code == 'invalid-email') {
        throw AuthException('Endereço de E-mail inválido!');
      } else {
        throw AuthException('Erro desconhecido durante o login!\n' + e.code);
      }
    }
  }

  resetPassword(String email) async {
    try {
      await _auth.sendPasswordResetEmail(email: email);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        throw AuthException('E-mail não encontrado. Cadastre-se!');
      } else {
        throw AuthException(
            'Erro desconhecido durante a recuperação de senha!\n' + e.code);
      }
    }
  }

  logout() async {
    await _auth.signOut();
    _getUser();
  }
}
