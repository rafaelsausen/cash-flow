import 'package:cash_flow/components/rb_button.dart';
import 'package:cash_flow/components/rb_checkbox.dart';
import 'package:cash_flow/components/rb_snack_bar.dart';
import 'package:cash_flow/components/rb_text_field.dart';
import 'package:cash_flow/constants/layout_constants.dart';
import 'package:cash_flow/modules/configurations/helpers/globals.dart';
import 'package:cash_flow/modules/login/components/gradient_background.dart';
import 'package:cash_flow/modules/login/screens/forgot_password.dart';
import 'package:cash_flow/modules/login/screens/sign_up.dart';
import 'package:cash_flow/modules/login/services/auth_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginView extends StatefulWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  final _user = TextEditingController();
  final _password = TextEditingController();
  bool rememberMe = false;

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    final size = mediaQuery.size;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SingleChildScrollView(
        reverse: false,
        child: SizedBox(
          height: size.height,
          child: Stack(
            children: <Widget>[
              const GradientBackgroundWidget(),
              Padding(
                padding: const EdgeInsets.all(16),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _buildEmailTF(),
                    _buildPasswordTF(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        _buildRememberMeCbox(),
                        _buildForgotPasswordBtn(),
                      ],
                    ),
                    _buildLoginBtn(context),
                    _buildSignupBtn(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  RBCheckbox _buildRememberMeCbox() {
    return RBCheckbox(
      text: 'Lembrar-me',
      isChecked: rememberMe,
      onChangeFunction: (result) {
        rememberMe = result;
      },
    );
  }

  RBButton _buildLoginBtn(BuildContext context) {
    return RBButton(
      key: const Key('signInButton'),
      text: "Entrar",
      onPressed: _initSession,
    );
  }

  Widget _buildEmailTF() {
    return RBTextField(
      textEditingController: _user,
      hintText: 'E-mail',
      icon: Icons.email_outlined,
    );
  }

  Widget _buildPasswordTF() {
    return RBTextField(
      textEditingController: _password,
      hintText: 'Senha',
      icon: Icons.lock_outline,
      obscureText: true,
    );
  }

  Widget _buildForgotPasswordBtn() {
    return TextButton(
      onPressed: () => Navigator.of(context).push(
          MaterialPageRoute(builder: (context) => const ForgotPasswordView())),
      //padding: const EdgeInsets.only(right: 0.0),
      child: const Text(
        'Esqueceu a senha?',
        style: rbLabelTextStyle,
      ),
    );
  }

  Widget _buildSignupBtn() {
    return GestureDetector(
      onTap: () => Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => const SignUpView())),
      child: RichText(
        text: const TextSpan(
          children: [
            TextSpan(
              text: 'Não possui uma conta? ',
              style: rbLabelTextStyle,
            ),
            TextSpan(
              text: 'Cadastrar',
              style: TextStyle(
                color: Colors.white,
                fontSize: 15.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }

  _initSession() async {
    try {
      await context.read<AuthService>().login(
            _user.text,
            _password.text,
          );
      rbSnackBar(
        context: context,
        message: "Seja bem vindo(a), $userName",
        color: Colors.green,
      );
    } on AuthException catch (e) {
      rbSnackBar(context: context, message: e.message, color: Colors.red);
    }
  }
}
