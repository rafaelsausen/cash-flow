import 'package:cash_flow/components/rb_button.dart';
import 'package:cash_flow/components/rb_snack_bar.dart';
import 'package:cash_flow/components/rb_text_field.dart';
import 'package:cash_flow/constants/layout_constants.dart';
import 'package:cash_flow/modules/login/components/gradient_background.dart';
import 'package:cash_flow/modules/login/services/auth_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class ForgotPasswordView extends StatefulWidget {
  const ForgotPasswordView({Key? key}) : super(key: key);

  @override
  _ForgotPasswordViewState createState() => _ForgotPasswordViewState();
}

class _ForgotPasswordViewState extends State<ForgotPasswordView> {
  // Controllers
  final formKey = GlobalKey<FormState>();
  final _user = TextEditingController();

  //Screen
  bool isLogin = true;
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: Form(
          key: formKey,
          child: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: Stack(
              children: <Widget>[
                const GradientBackgroundWidget(),
                SizedBox(
                  height: double.infinity,
                  child: SingleChildScrollView(
                    physics: const AlwaysScrollableScrollPhysics(),
                    padding: const EdgeInsets.symmetric(
                      horizontal: 16,
                      vertical: 120,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        const SizedBox(height: 30.0),
                        _buildEmailTF(),
                        const SizedBox(
                          height: 30.0,
                        ),
                        _buildLoginBtn(context),
                        _buildSendResetPasswordBtn(),
                        _buildReturnBtn(),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildEmailTF() {
    return RBTextField(
      textEditingController: _user,
      hintText: 'Insira seu E-mail',
      icon: Icons.email_outlined,
    );
  }

  RBButton _buildLoginBtn(BuildContext context) {
    return RBButton(
      key: const Key('signInButton'),
      text: "Recuperar Conta",
      iconStart: Icons.check,
      onPressed: () {
        if (isValid()) {
          resetPassword();
        }
      },
    );
  }

  Widget _buildReturnBtn() {
    return GestureDetector(
      onTap: () => Navigator.pop(context),
      child: RichText(
        text: const TextSpan(
          children: [
            TextSpan(
              text: 'Retornar para a tela de login.',
              style: rbLabelTextStyle,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildSendResetPasswordBtn() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 25.0),
      width: double.infinity,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: (loading)
              ? [
                  const Padding(
                      padding: EdgeInsets.all(16),
                      child: SizedBox(
                        width: 24,
                        height: 24,
                        child: CircularProgressIndicator(
                          color: Colors.purple,
                        ),
                      ))
                ]
              : [
                  const Icon(
                    Icons.check,
                    color: Colors.purple,
                  ),
                  const Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Text(
                      'RECUPERAR CONTA',
                      style: TextStyle(
                        color: Colors.purple,
                        letterSpacing: 1.5,
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'OpenSans',
                      ),
                    ),
                  )
                ],
        ),
        onPressed: () {
          if (isValid()) {
            resetPassword();
          }
        },
      ),
    );
  }

  resetPassword() async {
    setState(() => loading = true);
    try {
      await context.read<AuthService>().resetPassword(_user.text);
      rbSnackBar(
          context: context,
          message:
              "Um e-mail de para recuperação da sua conta foi enviado. Verifique seu e-mail.",
          color: Colors.blue);
      Navigator.pop(context);
      setState(() => loading = false);
    } on AuthException catch (e) {
      setState(() => loading = false);
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text(e.message)));
    }
  }

  bool isValid() {
    if (_user.text.isEmpty) {
      rbSnackBar(
          context: context, message: "Informe o login.", color: Colors.red);
      return false;
    }
    return true;
  }
}
