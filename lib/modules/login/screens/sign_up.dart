import 'package:cash_flow/components/rb_snack_bar.dart';
import 'package:cash_flow/components/rb_text_field.dart';
import 'package:cash_flow/constants/layout_constants.dart';
import 'package:cash_flow/modules/login/components/gradient_background.dart';
import 'package:cash_flow/modules/login/services/auth_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class SignUpView extends StatefulWidget {
  const SignUpView({Key? key}) : super(key: key);

  @override
  _SignUpViewState createState() => _SignUpViewState();
}

class _SignUpViewState extends State<SignUpView> {
  // Controllers
  final formKey = GlobalKey<FormState>();
  final _user = TextEditingController();
  final _password = TextEditingController();

  //Screen
  bool isLogin = true;
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: Form(
          key: formKey,
          child: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: Stack(
              children: <Widget>[
                const GradientBackgroundWidget(),
                SizedBox(
                  height: double.infinity,
                  child: SingleChildScrollView(
                    physics: const AlwaysScrollableScrollPhysics(),
                    padding: const EdgeInsets.symmetric(
                      horizontal: 40.0,
                      vertical: 120.0,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        const SizedBox(height: 30.0),
                        _buildEmailTF(),
                        const SizedBox(
                          height: 30.0,
                        ),
                        _buildPasswordTF(),
                        _buildSignInBtn(),
                        _buildReturnBtn(),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildEmailTF() {
    return RBTextField(
      keyboardType: TextInputType.emailAddress,
      textEditingController: _user,
      hintText: 'E-mail',
      icon: Icons.email_outlined,
    );
  }

  Widget _buildPasswordTF() {
    return RBTextField(
      textEditingController: _password,
      hintText: 'Insira sua senha',
      icon: Icons.lock_outline,
      obscureText: true,
    );
  }

  Widget _buildSignInBtn() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 25.0),
      width: double.infinity,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: (loading)
              ? [
                  const Padding(
                      padding: EdgeInsets.all(16),
                      child: SizedBox(
                        width: 24,
                        height: 24,
                        child: CircularProgressIndicator(
                          color: Colors.purple,
                        ),
                      ))
                ]
              : [
                  const Icon(
                    Icons.check,
                    color: Colors.purple,
                  ),
                  const Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Text(
                      'CRIAR CONTA',
                      style: TextStyle(
                        color: Colors.purple,
                        letterSpacing: 1.5,
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'OpenSans',
                      ),
                    ),
                  )
                ],
        ),
        onPressed: () {
          if (formKey.currentState!.validate()) {
            if (isValid()) {
              register();
            }
          }
        },
      ),
    );
  }

  Widget _buildReturnBtn() {
    return GestureDetector(
      onTap: () => Navigator.pop(context),
      child: RichText(
        text: const TextSpan(
          children: [
            TextSpan(
              text: 'Retornar para a tela de login.',
              style: rbLabelTextStyle,
            ),
          ],
        ),
      ),
    );
  }

  register() async {
    setState(() => loading = true);
    try {
      await context.read<AuthService>().register(_user.text, _password.text);
      rbSnackBar(
          context: context,
          message:
              "Um e-mail de confirmação foi enviado. Verifique seu e-mail.",
          color: Colors.blue);

      Navigator.pop(context);
      setState(() => loading = false);
    } on AuthException catch (e) {
      setState(() => loading = false);
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text(e.message)));
    }
  }

  bool isValid() {
    if (_user.text.isEmpty) {
      rbSnackBar(
          context: context, message: "Informe o login.", color: Colors.red);
      return false;
    }
    if (_password.text.isEmpty) {
      rbSnackBar(
          context: context, message: "Informe a senha.", color: Colors.red);
      return false;
    }
    if (_password.text.length < 6) {
      rbSnackBar(
          context: context,
          message: "A senha precisa ter no mínimo 6 caracteres.",
          color: Colors.red);
      return false;
    }
    return true;
  }
}
