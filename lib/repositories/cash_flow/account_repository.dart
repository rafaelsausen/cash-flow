import 'package:cash_flow/models/cash_flow/account_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class AccountRepository {
  void saveAccount(
    Account account,
  ) async {
    try {
      await FirebaseFirestore.instance
          .collection('accounts')
          .doc(account.id.toString())
          .set(account.toJson());
    } catch (error) {
      throw 'There was an error trying to save an Account: ' + error.toString();
    }
  }

  Future<QuerySnapshot> getAllAccounts() {
    return FirebaseFirestore.instance
        .collection('accounts')
        .orderBy('id')
        .get();
  }
}
