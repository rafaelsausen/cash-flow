import 'package:cash_flow/models/cash_flow/category_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class CategoryRepository {
  void saveCategory(
    Category category,
  ) async {
    try {
      await FirebaseFirestore.instance
          .collection('categories')
          .doc(category.id.toString())
          .set(category.toJson());
    } catch (error) {
      throw 'There was an error trying to save a Category: ' + error.toString();
    }
  }

  Future<QuerySnapshot> getAllCategories() {
    return FirebaseFirestore.instance
        .collection('categories')
        .orderBy('id')
        .get();
  }
}
