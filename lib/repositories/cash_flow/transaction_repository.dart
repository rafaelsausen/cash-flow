import 'package:cash_flow/models/cash_flow/transaction_model.dart'
    as my_transactions;
import 'package:cash_flow/providers/transaction_provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class TransactionRepository {
  Future<bool> saveTransaction(
    my_transactions.Transaction transaction,
  ) async {
    try {
      TransactionProvider transactionProvider = TransactionProvider.instance;
      Map<dynamic, Object?> idExists = await transactionProvider.queryRow(id: transaction.id);
      if (idExists.isNotEmpty) {
        // Update
        await transactionProvider.update(transaction.toJson());
      } else {
        // Insert
        await transactionProvider.insert(transaction.toJson());
      }
      await FirebaseFirestore.instance
          .collection('transactions')
          .doc(transaction.id.toString())
          .set(transaction.toJson());
      return true;
    } catch (error) {
      throw "There was an error trying to save a Transaction: \n"
          "${error.toString()}\n"
          "${transaction.toJson().toString()}";
    }
  }

  Future<int> getNextId() async {
    int id = 0;
    QuerySnapshot snapshot = await FirebaseFirestore.instance
        .collection('transactions')
        .orderBy('id')
        .limitToLast(1)
        .get();
    if (snapshot.docs.isNotEmpty) {
      id = snapshot.docs.first['id'];
    }
    return id += 1;
  }

  Future<bool> deleteTransaction(String id) async {
    try {
      TransactionProvider transactionProvider = TransactionProvider.instance;
      await transactionProvider.delete(id);
      await FirebaseFirestore.instance.collection('transactions')
          .doc(id)
          .delete();
      return true;
    } catch(e) {
      return false;
    }
  }
}
