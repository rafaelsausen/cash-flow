import 'package:cash_flow/models/cash_flow/totals_model.dart';
import 'package:cash_flow/providers/transaction_provider.dart';

class TotalsRepository {
  final Totals _totals = Totals(credit: 0, debit: 0, total: 0);

  Future<Totals> calculateTotals() async {
    Map<dynamic, Object?> sumCredit = await TransactionProvider.instance.queryColumnSum('credit');
    Map<dynamic, Object?> sumDebit = await TransactionProvider.instance.queryColumnSum('debit');
    _totals.credit = double.parse(sumCredit['SUM(credit)'].toString());
    _totals.debit = double.parse(sumDebit['SUM(debit)'].toString());
    _totals.total = _totals.credit - _totals.debit;
    return _totals;
  }
}