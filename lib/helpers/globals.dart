import 'package:cash_flow/models/cash_flow/account_model.dart';
import 'package:cash_flow/models/cash_flow/category_model.dart';

List<Account> accountList = [];
List<Category> categoryList = [];
List<String> allAccountDescriptions = [];
List<String> allCategoryDescriptions = [];
