import 'package:intl/intl.dart';

String convertDateToString(DateTime myDate) {
  return DateFormat.yMd('pt_Br')
      .format(myDate)
      .toString();
}