import 'package:flutter/material.dart';

//Primary Color
const rbPrimaryColor = Colors.orange;

const rbTextFieldColor = rbPrimaryColor;
//Color White
const rbBackgroundColor = Colors.white;

final rbBoxDecorationStyle = BoxDecoration(
  color: rbTextFieldColor,
  borderRadius: BorderRadius.circular(10.0),
  boxShadow: const [
    BoxShadow(
      color: Colors.black12,
      blurRadius: 6.0,
      offset: Offset(0, 2),
    ),
  ],
);

final rbRedBoxDecorationStyle = BoxDecoration(
  color: Colors.red,
  borderRadius: BorderRadius.circular(10.0),
  boxShadow: const [
    BoxShadow(
      color: Colors.black12,
      blurRadius: 6.0,
      offset: Offset(0, 2),
    ),
  ],
);

final rbGreenBoxDecorationStyle = BoxDecoration(
  color: Colors.green,
  borderRadius: BorderRadius.circular(10.0),
  boxShadow: const [
    BoxShadow(
      color: Colors.black12,
      blurRadius: 6.0,
      offset: Offset(0, 2),
    ),
  ],
);

const rbHintTextStyle = TextStyle(
  color: Colors.white,
);

const rbLabelTextStyle = TextStyle(
  fontSize: 18,
  color: Colors.white,
);

// Main Button
const rbButtonText = Colors.white;
const rbButtonColor = Color(0xFF3A5599);