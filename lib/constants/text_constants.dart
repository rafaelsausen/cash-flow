String applicationName = "Cash Flow";

const String tableTransactions = 'transactions';
const String columnId = 'id';
const String columnDescription = 'description';
const String columnDebit = 'debit';
const String columnCredit = 'credit';
const String columnDate = 'date';
const String columnCategory = 'category';
const String columnAccount = 'account';
const String columnTransferBetweenAccounts = 'transfer_between_accounts';