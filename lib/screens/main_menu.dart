import 'package:cash_flow/blocs/totals_bloc.dart';
import 'package:cash_flow/blocs/totals_events.dart';
import 'package:cash_flow/components/rb_list_item_slidable.dart';
import 'package:cash_flow/components/rb_navigator.dart';
import 'package:cash_flow/components/rb_progress.dart';
import 'package:cash_flow/constants/text_constants.dart';
import 'package:cash_flow/helpers/globals.dart';
import 'package:cash_flow/models/cash_flow/account_model.dart';
import 'package:cash_flow/models/cash_flow/category_model.dart';
import 'package:cash_flow/modules/login/services/auth_service.dart';
import 'package:cash_flow/repositories/cash_flow/account_repository.dart';
import 'package:cash_flow/repositories/cash_flow/category_repository.dart';
import 'package:cash_flow/screens/cash_flow/transaction/transaction_form.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:cash_flow/models/cash_flow/transaction_model.dart'
    as my_transactions;
import 'package:flutter_bloc/flutter_bloc.dart';

class TransactionList extends StatefulWidget {
  const TransactionList({
    Key? key,
  }) : super(key: key);

  @override
  State<TransactionList> createState() => _TransactionListState();
}

class _TransactionListState extends State<TransactionList> {
  final Stream _stream = FirebaseFirestore.instance
      .collection('transactions')
      .orderBy('date', descending: true)
      .snapshots();

  String total = '0';

  @override
  void initState() {
    BlocProvider.of<TotalsBloc>(context).add(LoadTotalsEvent());
    getAccounts();
    getCategories();
    super.initState();
  }

  getAccounts() async {
    QuerySnapshot accounts = await AccountRepository().getAllAccounts();
    for (var element in accounts.docs) {
      Account account =
          Account.fromJson(element.data() as Map<String, dynamic>);
      accountList.add(account);
      allAccountDescriptions.add(account.description);
    }
  }

  getCategories() async {
    QuerySnapshot categories = await CategoryRepository().getAllCategories();
    for (var element in categories.docs) {
      Category category =
          Category.fromJson(element.data() as Map<String, dynamic>);
      categoryList.add(category);
      allCategoryDescriptions.add(category.description);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        floatHeaderSlivers: true,
        headerSliverBuilder: (context, innerBoxIsScrolled) => [
          SliverAppBar(
            expandedHeight: 200,
            flexibleSpace: FlexibleSpaceBar(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(applicationName),
                      Text(BlocProvider.of<TotalsBloc>(context, listen: true)
                              .state
                              .totals
                              .total
                              .toStringAsFixed(2)
                          ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      GestureDetector(
                        child: const Icon(Icons.exit_to_app, color: Colors.white),
                        onTap: () => context.read<AuthService>().logout(),
                      ),
                    ],
                  )
                ],
              ),
            ),
            floating: true,
            snap: true,
            pinned: true,
          )
        ],
        body: StreamBuilder(
          stream: _stream,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasError) {
              return Text('Erro: ${snapshot.error}');
            }
            switch (snapshot.connectionState) {
              case ConnectionState.none:
                return const Text("No connection.");
              case ConnectionState.waiting:
                return const RBProgress();
              default:
                return ListView(
                  shrinkWrap: true,
                  children:
                      snapshot.data.docs.map<Widget>((DocumentSnapshot doc) {
                    my_transactions.Transaction transaction =
                        my_transactions.Transaction.fromJson(
                            doc.data() as Map<String, dynamic>);
                    return Container(
                      child: ListItemSlidable(
                        transaction: transaction,
                        context: context,
                      ),
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(
                              width: 1, color: Theme.of(context).primaryColor),
                        ),
                      ),
                      // This will create top borders
                    );
                  }).toList(),
                );
            }
          },
        ),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          FloatingActionButton(
            heroTag: 'AddCredit',
            backgroundColor: Colors.green,
            onPressed: () => addTransaction(debit: false),
            tooltip: 'Add a Credit',
            child: const Icon(Icons.add),
          ),
          FloatingActionButton(
            heroTag: 'AddDebit',
            backgroundColor: Colors.red,
            onPressed: () => addTransaction(debit: true),
            tooltip: 'Add a Debit',
            child: const Icon(Icons.remove),
          ),
        ],
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  addTransaction({required bool debit}) {
    rbNavigator(
      context,
      TransactionForm(
        debit: debit,
        currentTransaction: my_transactions.newTransaction(),
      ),
    );
  }
}
