import 'package:cash_flow/blocs/totals_bloc.dart';
import 'package:cash_flow/blocs/totals_events.dart';
import 'package:cash_flow/components/rb_dropdown_button.dart';
import 'package:cash_flow/components/rb_snack_bar.dart';
import 'package:cash_flow/components/rb_text_field.dart';
import 'package:cash_flow/constants/layout_constants.dart';
import 'package:cash_flow/helpers/globals.dart';
import 'package:cash_flow/models/cash_flow/account_model.dart';
import 'package:cash_flow/models/cash_flow/category_model.dart';
import 'package:cash_flow/repositories/cash_flow/transaction_repository.dart';
import 'package:flutter/material.dart';
import 'package:cash_flow/models/cash_flow/transaction_model.dart'
    as my_transactions;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class TransactionForm extends StatefulWidget {
  const TransactionForm({
    required this.currentTransaction,
    required this.debit,
    Key? key,
  }) : super(key: key);

  final my_transactions.Transaction currentTransaction;
  final bool debit;

  @override
  State<TransactionForm> createState() => _TransactionFormState();
}

class _TransactionFormState extends State<TransactionForm> {
  final String screenTitle = "Transaction Form";

  final TextEditingController _description = TextEditingController();
  final TextEditingController _value = TextEditingController();
  final TextEditingController _date = TextEditingController();

  late String category;
  late String account;

  @override
  void initState() {
    _description.text = widget.currentTransaction.description;
    if (widget.debit == true) {
      if (widget.currentTransaction.debit > 0) {
        _value.text = widget.currentTransaction.debit.toStringAsFixed(2);
      }
    } else {
      if (widget.currentTransaction.credit > 0) {
        _value.text = widget.currentTransaction.credit.toStringAsFixed(2);
      }
    }
    _date.text = DateFormat('dd/MM/yyyy').format(widget.currentTransaction.date);

    Account selectedAccount = accountList.firstWhere((element) => element.id == widget.currentTransaction.account);
    account = selectedAccount.description;

    Category selectedCategory = categoryList.firstWhere((element) => element.id == widget.currentTransaction.category);
    category = selectedCategory.description;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(screenTitle),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                RBTextField(
                  textEditingController: _description,
                  hintText: 'Description',
                  icon: Icons.description,
                ),
                //_buildDescriptionTF(),
                RBTextField(
                  textEditingController: _value,
                  hintText: 'Value',
                  keyboardType: TextInputType.number,
                  icon: Icons.monetization_on,
                  decoration: widget.debit
                      ? rbRedBoxDecorationStyle
                      : rbGreenBoxDecorationStyle,
                ),
                //_buildValueTF(),
                RBTextField(
                  textEditingController: _date,
                  hintText: 'Date',
                  icon: Icons.calendar_month,
                  onTap: () async {
                    await _showDatePicker(context);
                  },
                ),
                RBDropdownButton(
                  selectedValue: account,
                  items: allAccountDescriptions,
                  onChangeFunction: (newText) {
                    account = newText.toString();
                    //widget.currentTransaction.account = int.parse(newText.toString());
                  },
                  icon: Icons.account_balance,
                ),
                RBDropdownButton(
                  selectedValue: category,
                  items: allCategoryDescriptions,
                  onChangeFunction: (newText) {
                    category = newText.toString();
                    //widget.currentTransaction.category = int.parse(newText.toString());
                  },
                  icon: Icons.category,
                ),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          FloatingActionButton(
            heroTag: 'Confirm',
            backgroundColor: Colors.green,
            onPressed: () {
              _validateAndSave();
            },
            tooltip: 'Confirm',
            child: const Icon(Icons.check),
          ),
          FloatingActionButton(
            heroTag: 'Cancel',
            backgroundColor: Colors.red,
            onPressed: () {
              Navigator.of(context).pop();
            },
            tooltip: 'Cancel',
            child: const Icon(Icons.cancel),
          ),
        ],
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Future<void> _showDatePicker(BuildContext context) async {
    DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2000),
        lastDate: DateTime(2101));

    if (pickedDate != null) {
      String formattedDate = DateFormat('dd/MM/yyyy').format(pickedDate);

      setState(() {
        _date.text = formattedDate; //set output date to TextField value.
      });
    }
  }

  void _validateAndSave() async {
    if (_validateFields() == false) {
      return;
    }
    await _fillFields();
    await TransactionRepository().saveTransaction(widget.currentTransaction);
    context.read<TotalsBloc>().add(LoadTotalsEvent());
    Navigator.of(context).pop();
  }

  bool _validateFields() {
    if (_description.text.isEmpty) {
      rbSnackBar(
          context: context,
          message: "Fill the description!",
          color: Colors.red);
      return false;
    }
    if (_value.text.isEmpty) {
      rbSnackBar(
          context: context, message: "Fill the value!", color: Colors.red);
      return false;
    }
    return true;
  }

  Future<void> _fillFields() async {
    if (widget.currentTransaction.id == 0) {
      widget.currentTransaction.id = await TransactionRepository().getNextId();
    }
    widget.currentTransaction.description = _description.text;
    if (widget.debit) {
      widget.currentTransaction.debit =
          double.parse(double.parse(_value.text).toStringAsExponential(2));
      widget.currentTransaction.credit = 0;
    } else {
      widget.currentTransaction.credit =
          double.parse(double.parse(_value.text).toStringAsExponential(2));
      widget.currentTransaction.debit = 0;
    }
    Account selectedAccount =
        accountList.firstWhere((element) => element.description == account);
    widget.currentTransaction.account = selectedAccount.id;
    Category selectedCategory =
        categoryList.firstWhere((element) => element.description == category);
    widget.currentTransaction.category = selectedCategory.id;
    widget.currentTransaction.date = DateFormat('dd/MM/yyyy').parse(_date.text);
  }
}
