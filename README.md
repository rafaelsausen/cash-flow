# Cash Flow

Projeto criado para praticar meu conhecimento em Flutter e controlar minhas contas mensais.
Pretendo implementar inicialmente:
- Controle das entradas e saídas de dinheiro no caixa.
- Controle de contas diferentes. Ex: Carteira, Conta Corrente, Conta Poupança, etc.
- Separar as contas por categoria. Ex: Mercado, Farmácia, etc.

Em um segundo momento implementar:
- Contas mensais. Ex: Luz, Água, Internet.
- Parcelas. Ex: Compras parceladas em diferentes lojas.
- Cartões de crédito. Ex: Controlar valores gastos no cartão de crédito.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
